$(document).ready(function () {

    prepareHeader("View History");

    $("body").append([
        buildMyDeployModal()
    ]);

    $("#mainContainer").append([
        ,
        buildDiv("jobsTable")
    ]);

    ciService.getJobs(jobs => updateJobsTable(jobs));

    function updateJobsTable(jobs) {

        $("#jobs").append(
            buildH2(jobs.length + " CI jobs"),
            createTableWithRows(
                jobs.map(toJobRowInfo),
                ["Name", "Last Build", "Happened", "Properties"]
            )
        );

        jobs.forEach(job => ciService.getLastBuild(job.name, handleLastBuildInfo));

    }

    function handleLastBuildInfo(lastBuild) {
        if (lastBuild) {
            var lastBuildButtons = [
                buildHref(lastBuild.url, lastBuild.id),
                parseResult(lastBuild.result),
                //buildLinkToJob(job),

                buildLinkToHistory(lastBuild),
                buildLinkToHistoryStats(lastBuild),
                createCIRebuildJobButton(lastBuild.name, lastBuild.parameters, lastBuild.building)
            ];

            $("#lastBuildOf" + sanitize(lastBuild.name)).append(
                placeVertically(lastBuildButtons)
            );

            $("#happenedOf" + sanitize(lastBuild.name)).append(
                timeSince(new Date(lastBuild.timestamp)) + " ago"
            );

            $("#propertiesOf" + sanitize(lastBuild.name)).append(
                createTableWithRows(
                    lastBuild.parameters.map(param => [param.name, param.value]),
                    null,
                    TABLE_STYLES.STRIPED
                )
            );
        }
    }

    function sanitize(name) {
        return name.replace(".", "_");
    }

    function toJobRowInfo(job) {
        return [
            buildHref(job.url, job.name),
            buildDiv("lastBuildOf" + sanitize(job.name)),
            buildDiv("happenedOf" + sanitize(job.name)),
            buildDiv("propertiesOf" + sanitize(job.name))
        ];
    }


    function buildLinkToHistory(job) {
        return buildHref("jobHistory.html?job=" + job.name, "History");
    }

    function buildLinkToHistoryStats(lastBuild) {
        return buildHref("jobHistoryStats.html?job=" + lastBuild.name, "History Stats");
    }

    function buldLinkToBuild(lastBuild) {
        return buildHref(lastBuild.url + "console", lastBuild.id);
    }


    function parseResult(result) {
        if (result) {
            return result != "SUCCESS" ? highLighted(result) : result;
        } else {
            return "Running";
        }
    }

});
