function highLighted(txt) {
    return "<i><b>" + txt + "</b></i>"
}

function p(content, id) {
    var element = document.createElement("p");
    if (id) {
        element.id = id;
    }
    if (content) {
        $(element).append(content);
    }
    return element;

}

function b(txt) {
    var b = document.createElement("b");
    b.innerHTML = txt;
}

var TABLE_STYLES = {
    "CONDENSED": "table table-condensed",
    "STRIPED": "table table-striped",
    "BORDERED": "table table-bordered"
};


function prepareHeader(title) {

    var head = d3.select("head").attr("lang", "en");
    head.append("meta").attr("charset", "utf-8");
    head.append("meta").attr("http-equiv", "X-UA-Compatible").attr("content", "IE=edge");
    head.append("meta").attr("name", "viewport").attr("content", "width=device-width, initial-scale=1");
    if (title) {
        head.append("title").html(title);
    }
    head.append("link").attr("href", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css").attr("rel", "stylesheet");

}

function buildMainContainer() {
    return buildDiv("mainContainer", "container");
}

function addToBody(elements) {
    $("body").append(elements);
}

function addToMainContainer(elements) {
    $("#mainContainer").append(elements);
}

// function addChildToNode(parent) {
//     return child => {
//         if (child instanceof Node) {
//             parent.appendChild(child);
//         } else {
//             parent.innerHTML = child;
//         }
//     }
// }

// function getById(id) {
//     return document.getElementById(id);
// }

function placeVerticallyUsingP(content, id) {
    return buildDiv(id, null,
        content.map(node => p(node))
    )
        ;
}

function addLengthAtTheStartOfTheTitle(title, elements) {
    $("#" + title).html((i, origText) =>
        elements.length + " " + origText
    )
    ;
}

function addLengthAtTheEndOfTheTitle(title, entries) {
    $("#" + title).html((i, origText) =>
    origText + " (" + entries.length + " found)"
)
    ;
}

function buildHref(href, text) {
    if (href) {
        var link = document.createElement("a");
        link.setAttribute("href", href);
        link.setAttribute("target", "_blank");
        link.setAttribute("role", "button");
        link.className = "btn btn-default btn-lg";

        var linkText = document.createTextNode(text);
        link.appendChild(linkText);
        return link;
    } else {
        return text;
    }
}

function createCollapseElement(targetId, content, text, disabled) {
    return buildDiv(null, null,
        [
            createCollapseButton("#" + targetId, text, disabled),
            buildDiv(
                targetId,
                "collapse",
                [content]
            )
        ]
    );
}

function createCollapseButton(target, text, disabled) {
    var button = document.createElement("button");
    button.setAttribute("type", "button");
    button.setAttribute("data-toggle", "collapse");
    button.setAttribute("data-target", target);
    button.className = "btn btn-info";
    button.innerHTML = text;

    if (disabled) {
        button.setAttribute("disabled", "disabled");
    }

    return button;
}

function placeHorizontally(content, header, id) {
    return createTableWithRows([content], header, TABLE_STYLES.CONDENSED, id);
}

function placeVertically(content, header, id, style) {
    return createTableWithRows(partition(content, 1), header, style || TABLE_STYLES.CONDENSED, id);
}

function partition(array, n) {
    return array.length ? [array.splice(0, n)].concat(partition(array, n)) : [];
}

function timeSince(date) {
    var totalSeconds = Math.floor((new Date() - date) / 1000);

    var days = toDays(totalSeconds);
    var hours = toHours(totalSeconds) - (days * 24);
    var minutes = toMinutes(totalSeconds) - (toHours(totalSeconds) * 60);

    if (days > 0) {
        return days + " days, " + hours + " hours, " + minutes + " minutes";
    } else if (hours > 0) {
        return hours + " hours, " + minutes + " minutes";
    } else {
        return minutes + " minutes";
    }
}

function toMinutes(seconds) {
    return Math.floor(seconds / 60);
}

function toHours(seconds) {
    return Math.floor(seconds / (60 * 60));
}

function toDays(seconds) {
    return Math.floor(seconds / (60 * 60 * 24));
}

function gup(name, url) {
    if (!url) url = location.href;

    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}
function createButton(value, func, btnClass, disabled) {
    var button = document.createElement("input");
    button.type = "button";
    button.value = value;
    button.onclick = func;
    button.className = btnClass;
    //console.log("disabled " + disabled);

    if (disabled) {
        button.setAttribute("disabled", "disabled");
    }
    return button;
}

function bold(txt) {
    return "<b>" + txt + "</b>";
}

function buildHrefObj(href, text, openInNewTab, shouldLookLikeAButton) {
    return {
        href: href,
        text: text,
        openInNewTab: openInNewTab,
        shouldLookLikeAButton: shouldLookLikeAButton
    }
}

function populateDropDownListWithLinks(elements, listName) {
    $("#" + listName).append(
        elements
            .map(buildHrefFromObj)
            .map(buildLiWithHref)
    );
}

function createElement(name, id, klass) {
    var element = document.createElement(name);

    if (id) {
        element.id = id;
    }
    if (klass) {
        element.className = klass;
    }
    return element;
}

function buildHrefFromObj(hrefObj) {
    var link = document.createElement("a");
    link.setAttribute("href", hrefObj.href);
    if (hrefObj.openInNewTab) {
        link.setAttribute("target", "_blank");
    }

    if (hrefObj.shouldLookLikeAButton) {
        link.setAttribute("role", "button");
        link.className = "btn btn-default btn-lg";
    }

    if (hrefObj.text instanceof Node) {
        link.appendChild(hrefObj.text);
    } else {
        link.appendChild(document.createTextNode(hrefObj.text));
    }

    return link;
}

function buildH2(html, id) {

    var newH2 = document.createElement("h2")

    var h2 = d3.select(newH2)
        .html(html);

    if (id) {
        h2.attr("id", id);
    }

    return newH2;

//    return parent => {
//        var h2 = d3.select(parent).append("h2")
//            .html(html);
//
//        if(id) {
//            h2.attr("id", id);
//        }
//
//        return h2;
//    }
}

function buildDiv(id, klass, children) {
    var newDiv = document.createElement("div");
    var div = d3.select(newDiv)
        .attr("id", id);

    if (klass) {
        div.classed(klass, true);
    }

    if (children) {
        $(newDiv).append(children);
    }
    return newDiv;
}

function createTableWithRows(data, header, style, id) {
    var newDiv = document.createElement("div");
    var table = d3.select(newDiv).classed("table-responsive", true)
        .append("table").classed(style || TABLE_STYLES.BORDERED, true);

    // var table = d3.select(parentId).append("table")
    //     .classed(style, true);

    var thead = table.append("thead"),
        tbody = table.append("tbody");

    if (id) {
        table.attr("id", id);
    }

    if (header) {
        thead.append("tr")
            .selectAll("th")
            .data(header)
            .enter()
            .append("th")
            .text(c => c)
        ;
    }

    // create a row for each object in the data
    var rows = tbody.selectAll("tr")
        .data(data)
        .enter()
        .append("tr");

    // create a cell in each row for each column
    rows.selectAll("td")
        .data(row => row)
        .enter()
        .append("td")
        .attr("style", "font-family: Courier") // sets the font style
        .html(d => isNode(d) ? null : d)
        .filter(isNode)
        .append(d => d)
    ;

    // appendTableWithRows(newDiv, data, header, style || TABLE_STYLES.BORDERED, id);
    return newDiv;
}


var isNode = d => d instanceof Node;

function buildLiWithHref(href) {
    var li = document.createElement("li");
    li.appendChild(href);
    return li;
}

