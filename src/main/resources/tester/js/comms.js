function ciService() {
}

ciService.getJobs = function (callback) {
    performGet("/rest/jobs", callback);
};

ciService.getLastBuild = function (jobName, callback) {
    performGet("/rest/jobs/" + jobName + "/last-build", callback);
};

ciService.getBuildHistory = function (jobName, callback) {
    performGet("/rest/jobs/" + jobName + "/history", callback);
};

ciService.getBuildHistoryStats = function (jobName, callback) {
    performGet("/rest/jobs/" + jobName + "/history/stats", callback);
};

ciService.buildJob = function (jobName, params, callback) {
    performPost("/rest/jobs/" + jobName + "/build", params, callback);
};

function performGet(url, callback) {
    $.get(url, callback);
}

function performPost(url, data, callback) {

    $.ajax({
        url: url,
        "type": "POST",
        "data": JSON.stringify(data),
        "contentType": "application/json"
    }).then(callback).fail(function () {
        alert("Error while posting to " + url);
    });
}

function performPut(url, data, callback) {

    $.ajax({
        url: url,
        "type": "PUT",
        "data": JSON.stringify(data),
        "contentType": "application/json"
    }).then(callback).fail(function () {
        alert("Error while posting to " + url);
    });
}

function performDelete(url, data, callback) {
    $.ajax({
        url: url,
        "type": "DELETE",
        "data": JSON.stringify(data),
        "contentType": "application/json"
    }).then(callback).fail(function () {
        alert("Error while deleting " + url);
    });
}