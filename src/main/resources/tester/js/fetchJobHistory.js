$(document).ready(function () {
    var job = gup("job", document.location.href);

    prepareHeader("History of " + job);

    $("body").append([
        buildMyDeployModal()
    ]);

    $("#mainContainer").append([]);

    ciService.getBuildHistory(job, entries =>
        $("#history").append(
            buildH2("History of " + job),
            createTableWithRows(
                entries.map(parseHistory(job)),
                ["Name", "Result", "Happened", "Properties"]
            )
        )
    );


    function parseHistory(jobName) {
        return entry => [
            buildHref(entry.url + "console", entry.id),
            //entry.id,
            placeVertically([
                parseResult(entry.result, entry.building),
                createCIRebuildJobButton(jobName, entry.parameters, entry.building)
            ]),
            //parseResult(entry.result),
            timeSince(new Date(entry.timestamp)) + " ago",

            createTableWithRows(
                entry.parameters.map(param => [param.name, param.value]),
                null,
                TABLE_STYLES.STRIPED
            )
        ];
    }

    function parseResult(result, building) {
        if (result) {
            if (building) {
                result = "BUILDING";
            }

            return result != "SUCCESS" ? highLighted(result) : result;
        } else {
            return "Running";
        }
    }

});
