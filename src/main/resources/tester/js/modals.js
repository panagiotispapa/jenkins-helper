function buildMyDeployModal() {
    return buildModal(
        "myDeployModal",
        buildModalHeader("Deploying"),
        buildModalBody(
            placeVerticallyUsingP([
                "Your deployment has started",
                buildActiveLink("ciJobLink", "Go to CI", true)
            ]))
    );
}

function buildModal(id, header, body) {
    var modalDiv = buildDiv(
        id,
        "modal fade",
        [buildModalDialog(header, body)]
    );
    modalDiv.role = "dialog";
    return modalDiv;

}

function buildModalDialog(header, body) {
    return buildDiv(
        "",
        "modal-dialog",
        [buildModalContent(header, body)]
    );
}

function buildModalContent(header, body) {
    return buildDiv(
        "",
        "modal-content",
        [header, body]
    );
}

function buildModalBody(content) {
    return buildDiv(
        "",
        "modal-body",
        [content]
    );
}

function buildModalHeader(titleTxt) {
    return buildDiv(
        "",
        "modal-header",
        [
            buildCloseButton(),
            buildModalTitle(titleTxt)
        ]);
}

function buildModalTitle(txt) {
    var h4 = document.createElement("h4");
    h4.className = "modal-title";
    h4.innerHTML = txt;
    return h4;
}

function buildActiveLink(id, txt, openInNewTab) {
    var activeLink = document.createElement("a");
    activeLink.id = id;
    activeLink.href = "#";
    if (openInNewTab) {
        activeLink.target = "_blank";
    }
    activeLink.role = "button";
    activeLink.className = "btn btn-primary btn-lg active";
    activeLink.innerHTML = txt;
    return activeLink;
}

function buildCloseButton() {
    var button = document.createElement("button");

    button.type = "button";
    button.className = "close";
    button.setAttribute("data-dismiss", "modal");
    button.innerHTML = "&times;";

    return button;
}

function showModal(modal) {
    $(modal).modal({
        backdrop: false,
        show: true
    });
}

function hideModal(modal) {
    $(modal).modal('hide');
}


function showModalForCIDeployment(data) {
    console.log("data.queueId: " + data.queueId);
    console.log("data.task: " + data.task);
    console.log("data.executable: " + data.executable);

    $('#ciJobLink').attr("href", getActualDeploymentUrl(data));
    showModal('#myDeployModal');
}



function getActualDeploymentUrl(data) {
    return data.executable
        ? data.executable.url
        : data.task.url;
}
