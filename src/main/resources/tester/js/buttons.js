var BUTTON_STYLES = {
    "PRIMARY": "btn btn-primary",
    "WARNING": "btn btn-warning",
    "DANGER": "btn btn-danger"
};


function createCIRebuildJobButton(jobName, parameters, building) {
    var enabled = !building;

    return createButton("Rebuild", function () {

        parameters.forEach(function (param) {
            console.log("name " + param.name + " value " + param.value);
        });

        ciService.buildJob(jobName, parameters, showModalForCIDeployment);


    }, BUTTON_STYLES.WARNING, !enabled)
}

