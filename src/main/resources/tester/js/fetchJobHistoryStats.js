$(document).ready(function () {
    var job = gup("job", document.location.href);


    d3.select("body").append("h2").text(job);
    d3.select("body").append("div").attr("id", "bar-demo");


    ciService.getBuildHistoryStats(job, drawHistoryStats);


    function drawHistoryStats(stats) {
        var data = stats.entries;
        //    var barWidth = 10;
        //    var width = (barWidth + 2) * data.length;

        var width = 1200;
        var barWidth = width / data.length - 2;


        var height = 550;

        var maxDuration = d3.max(data, datum => datum.duration);
        var x = d3.scaleLinear().domain([0, data.length]).range([0, width]);
        var y = d3.scaleLinear().domain([0, maxDuration]).rangeRound([0, height]);


        // add the canvas to the DOM
        var barDemo = d3.select("#bar-demo").
        append("svg:svg").
        attr("width", width).
        attr("height", height);


        barDemo.
        append("svg:text").
        attr("x", 10).
        attr("y", 15).
        attr("fill", "#008000").
        text("Success: " + stats.successes);

        barDemo.
        append("svg:text").
        attr("x", 10).
        attr("y", 40).
        attr("fill", "#ff0000").
        text("Failure: " + stats.failures);

        barDemo.selectAll("a").
        data(data.reverse()).
        enter().
        append("svg:a").
        attr("xlink:href", entry => entry.url + "console").
        attr("xlink:show", "new").
        append("svg:rect").
        attr("x", (datum, index) => x(index)).
        attr("y", datum => height - y(datum.duration) + 60).
        attr("height", datum => y(datum.duration)).
        attr("width", barWidth).
        attr("fill", getColorBasedOnResult);
    }

    function getColorBasedOnResult(entry) {
        return entry.result == "SUCCESS" ? "#008000" : "#ff0000";
    }


});
