package com.panos.jenkinshelper.model;

import java.util.List;

public class JenkinsJobHistory {
    private List<JenkinsBuildEntry> builds;

    public List<JenkinsBuildEntry> getBuilds() {
        return builds;
    }

    public void setBuilds(List<JenkinsBuildEntry> builds) {
        this.builds = builds;
    }
}
