package com.panos.jenkinshelper.model;

import java.util.List;

public class JenkinsBuildEntry {
    private String id;
    private Long duration;
    private Integer number;
    private Integer queueId;
    private Long timestamp;
    private String result;
    private boolean building;
    private String builtOn;
    private String url;
    private String name;

    private List<JenkinsAction> actions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getQueueId() {
        return queueId;
    }

    public void setQueueId(Integer queueId) {
        this.queueId = queueId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getBuiltOn() {
        return builtOn;
    }

    public void setBuiltOn(String builtOn) {
        this.builtOn = builtOn;
    }

//    public List<JenkinsAction> getActions() {
//        return actions;
//    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setActions(List<JenkinsAction> actions) {
        this.actions = actions;
    }

    public JenkinsBuildEntryParameters getParameters() {
        return new JenkinsBuildEntryParameters(actions);
    }

    public Boolean forEnv(String envId) {
        return getParameters().extractParamValue("ENV_ID").map(envId::equals).orElse(false);
    }

    public Boolean forService(String service) {
        return getParameters().extractParamValue("SERVICES").map(service::equals).orElse(false);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBuilding() {
        return building;
    }

    public void setBuilding(boolean building) {
        this.building = building;
    }
}

