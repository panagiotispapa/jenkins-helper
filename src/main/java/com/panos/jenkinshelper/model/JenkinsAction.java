package com.panos.jenkinshelper.model;

import java.util.List;

public class JenkinsAction {

    private List<JenkinsParameterValue> parameters;

    public List<JenkinsParameterValue> getParameters() {
        return parameters;
    }

    public void setParameters(List<JenkinsParameterValue> parameters) {
        this.parameters = parameters;
    }

    public boolean hasParameters() {
        return parameters != null;
    }
}
