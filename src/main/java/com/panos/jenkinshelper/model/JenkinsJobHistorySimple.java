package com.panos.jenkinshelper.model;

import java.util.List;

public class JenkinsJobHistorySimple {
    private List<JenkinsBuildEntrySimple> builds;

    public List<JenkinsBuildEntrySimple> getBuilds() {
        return builds;
    }

    public void setBuilds(List<JenkinsBuildEntrySimple> builds) {
        this.builds = builds;
    }
}
