package com.panos.jenkinshelper.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class JenkinsBuildEntryParameters {
    private final List<JenkinsParameterValue> parameterValues;

    public JenkinsBuildEntryParameters(List<JenkinsAction> actions) {
        this.parameterValues = actions.stream().filter(JenkinsAction::hasParameters).findFirst().map(JenkinsAction::getParameters).orElseGet(Collections::emptyList);
    }

    public Optional<String> extractParamValue(String key) {
        return parameterValues.stream().filter(p -> key.equalsIgnoreCase(p.getName())).findAny().map(JenkinsParameterValue::getValue).filter(StringUtils::isNotBlank);
    }

    public List<JenkinsParameterValue> getParameterValues() {
        return parameterValues;
    }

}
