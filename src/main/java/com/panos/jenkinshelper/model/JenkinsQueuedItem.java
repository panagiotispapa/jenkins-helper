package com.panos.jenkinshelper.model;

import java.util.Optional;

public class JenkinsQueuedItem {
    private Integer id;
    private boolean blocked;
    private boolean cancelled;
    private boolean stuck;
    private Executable executable;
    private Task task;
    private Long inQueueSince;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isStuck() {
        return stuck;
    }

    public void setStuck(boolean stuck) {
        this.stuck = stuck;
    }

    public Executable getExecutable() {
        return executable;
    }

    public void setExecutable(Executable executable) {
        this.executable = executable;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Long getInQueueSince() {
        return inQueueSince;
    }

    public void setInQueueSince(Long inQueueSince) {
        this.inQueueSince = inQueueSince;
    }

    public boolean hasExecutable() {
        return executable != null;
    }

    public String getUrl() {
        return Optional.ofNullable(executable).map(Executable::getUrl).orElseGet(task::getUrl);
    }

    @Override
    public String toString() {
        return "JenkinsQueuedItem{" +
                "id=" + id +
                ", blocked=" + blocked +
                ", cancelled=" + cancelled +
                ", stuck=" + stuck +
                ", executable=" + executable +
                ", task=" + task +
                '}';
    }

    public static class Executable {
        private Integer number;
        private String url;

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "Executable{" +
                    "number=" + number +
                    ", url='" + url + '\'' +
                    '}';
        }
    }

    public static class Task {
        private String name;
        private String url;
        private String color;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "Task{" +
                    "name='" + name + '\'' +
                    ", url='" + url + '\'' +
                    ", color='" + color + '\'' +
                    '}';
        }
    }


}
