package com.panos.jenkinshelper.model;


import com.panos.jenkinshelper.Utils;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

public class JenkinsBuildHistoryStats {

    private final Integer successes;
    private final Integer failures;
    private final List<JenkinsBuildEntrySimple> entries;

    public JenkinsBuildHistoryStats(List<JenkinsBuildEntrySimple> entries) {
        this.entries = entries;

        final Map<Boolean, List<JenkinsBuildEntrySimple>> grouped = entries.stream().filter(Utils.p(JenkinsBuildEntrySimple::isBuilding).negate()).collect(groupingBy(entry -> entry.getResult().equalsIgnoreCase("SUCCESS")));

        successes = grouped.get(true).size();
        failures = grouped.get(false).size();

    }

    public Integer getSuccesses() {
        return successes;
    }

    public Integer getFailures() {
        return failures;
    }

    public List<JenkinsBuildEntrySimple> getEntries() {
        return entries;
    }
}

