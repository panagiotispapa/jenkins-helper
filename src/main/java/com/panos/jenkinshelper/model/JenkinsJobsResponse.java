package com.panos.jenkinshelper.model;

import java.util.List;

public class JenkinsJobsResponse {
    private List<JenkinsJob> jobs;

    public List<JenkinsJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<JenkinsJob> jobs) {
        this.jobs = jobs;
    }
}
