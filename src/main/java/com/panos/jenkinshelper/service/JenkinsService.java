package com.panos.jenkinshelper.service;

import com.panos.jenkinshelper.model.*;
import com.panos.jenkinshelper.repository.JenkinsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JenkinsService {

    private final JenkinsRepository jenkinsRepository;

    @Autowired
    public JenkinsService(JenkinsRepository jenkinsRepository) {
        this.jenkinsRepository = jenkinsRepository;
    }

    public String runJob(String jobName, List<JobParameter> parameters) {
        return jenkinsRepository.runJob(jobName, parameters);
    }

    public List<JenkinsJob> getJobs() {
        return jenkinsRepository.getJobs();
    }

    public Optional<JenkinsBuildEntry> getLastBuild(String jobName) {
        return jenkinsRepository.getLastBuild(jobName);
    }

    public List<JenkinsBuildEntry> getJobHistory(String jobName) {
        return jenkinsRepository.getJobHistory(jobName);
    }

    public JenkinsBuildHistoryStats getJobHistoryStats(String jobName) {
        return jenkinsRepository.getJobHistoryStats(jobName);
    }

}
