package com.panos.jenkinshelper;

import com.panos.jenkinshelper.spring.PreemptiveAuth;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.StandardHttpRequestRetryHandler;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static java.util.Collections.singletonList;
import static org.apache.http.conn.ssl.SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Value("${jenkins.host}")
    private String jenkinsServer;

    @Value("${jenkins.userId}")
    private String userId;

    @Value("${jenkins.apiKey}")
    private String apiKey;


    @Bean
    CloseableHttpClient ciHttpClient() throws GeneralSecurityException {
        //https://wiki.jenkins-ci.org/display/JENKINS/Authenticating+scripted+clients

        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        String updatedUserId = userId;
        credentialsProvider.setCredentials(new AuthScope(jenkinsServer, 443, AuthScope.ANY_REALM), new UsernamePasswordCredentials(updatedUserId, apiKey));

        return HttpClientBuilder.create()
                .setSSLSocketFactory(new SSLConnectionSocketFactory(new org.apache.http.ssl.SSLContextBuilder().loadTrustMaterial(null, (a, b) -> true).build(), ALLOW_ALL_HOSTNAME_VERIFIER))
                .setDefaultCredentialsProvider(credentialsProvider)
                .setRetryHandler(new StandardHttpRequestRetryHandler(1, true))
                .setDefaultRequestConfig(
                        RequestConfig.custom()
                                .setConnectionRequestTimeout((int) TimeUnit.SECONDS.toMillis(4))
                                .setConnectTimeout((int) TimeUnit.SECONDS.toMillis(4))
                                .setSocketTimeout((int) TimeUnit.SECONDS.toMillis(4)).build())
                .setMaxConnTotal(500)
                .setMaxConnPerRoute(50)
                .addInterceptorFirst(new PreemptiveAuth())
                .build();
    }

    @Bean
    public RestTemplate ciRestTemplate(@Qualifier(value = "mappingJackson2HttpMessageConverter") final MappingJackson2HttpMessageConverter converter) throws GeneralSecurityException {
        final ArrayList<MediaType> mediaTypes = new ArrayList<>(converter.getSupportedMediaTypes());
        mediaTypes.add(MediaType.TEXT_PLAIN);
        converter.setSupportedMediaTypes(mediaTypes);

        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(ciHttpClient()));

        restTemplate.setMessageConverters(singletonList(converter));
        return restTemplate;
    }

}
