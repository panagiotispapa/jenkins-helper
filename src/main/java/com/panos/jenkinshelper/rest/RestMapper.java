package com.panos.jenkinshelper.rest;

import com.panos.jenkinshelper.model.JenkinsQueuedItem;
import com.panos.jenkinshelper.rest.model.CIBuildView;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class RestMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(JenkinsQueuedItem.class, CIBuildView.class)
                .byDefault()
                .field("id", "queueId")
                .register();

    }
}
