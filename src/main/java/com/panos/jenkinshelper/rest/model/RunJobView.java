package com.panos.jenkinshelper.rest.model;

public class RunJobView {

    public RunJobView() {
    }

    public RunJobView(String url) {
        this.url = url;
    }

    public String url;
}
