package com.panos.jenkinshelper.rest.model;

public class CIJobExecutionView {
    private Integer number;
    private String url;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
