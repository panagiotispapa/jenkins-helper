package com.panos.jenkinshelper.rest.model;

public class CIBuildView {

    private Integer queueId;
    
    private CITaskView task;

    private CIJobExecutionView executable;

    public CITaskView getTask() {
        return task;
    }

    public void setTask(CITaskView task) {
        this.task = task;
    }

    public CIJobExecutionView getExecutable() {
        return executable;
    }

    public void setExecutable(CIJobExecutionView executable) {
        this.executable = executable;
    }

    public Integer getQueueId() {
        return queueId;
    }

    public void setQueueId(Integer queueId) {
        this.queueId = queueId;
    }
}
