package com.panos.jenkinshelper.rest.model;


import com.panos.jenkinshelper.model.JenkinsBuildEntry;
import com.panos.jenkinshelper.model.JenkinsParameterValue;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class JenkinsBuildEntryView {
    private String id;
    private Long duration;
    private Integer number;
    private Integer queueId;
    private Long timestamp;
    private String result;
    private boolean building;
    private String builtOn;
    private String url;
    private String name;

    private List<JenkinsParameterValue> parameters;


    public JenkinsBuildEntryView(String id, Long duration, Integer number, Integer queueId, Long timestamp, String result, boolean building, String builtOn, String url, String name, List<JenkinsParameterValue> parameters) {
        this.id = id;
        this.duration = duration;
        this.number = number;
        this.queueId = queueId;
        this.timestamp = timestamp;
        this.result = result;
        this.building = building;
        this.builtOn = builtOn;
        this.url = url;
        this.name = name;
        this.parameters = parameters.stream().filter(Objects::nonNull).collect(toList());
    }

    public static JenkinsBuildEntryView fromEntry(JenkinsBuildEntry entry) {
        return new JenkinsBuildEntryView(
                entry.getId(),
                entry.getDuration(),
                entry.getNumber(),
                entry.getQueueId(),
                entry.getTimestamp(),
                entry.getResult(),
                entry.isBuilding(),
                entry.getBuiltOn(),
                entry.getUrl(),
                entry.getName(),
                entry.getParameters().getParameterValues()
        );
    }

    public static JenkinsBuildEntryView fromEntry(Optional<JenkinsBuildEntry> maybeEntry) {
        return maybeEntry.map(JenkinsBuildEntryView::fromEntry).orElse(null);
    }


    public String getId() {
        return id;
    }

    public Long getDuration() {
        return duration;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getQueueId() {
        return queueId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getResult() {
        return result;
    }

    public String getBuiltOn() {
        return builtOn;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public List<JenkinsParameterValue> getParameters() {
        return parameters;
    }

    public boolean isBuilding() {
        return building;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setQueueId(Integer queueId) {
        this.queueId = queueId;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setBuilding(boolean building) {
        this.building = building;
    }

    public void setBuiltOn(String builtOn) {
        this.builtOn = builtOn;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParameters(List<JenkinsParameterValue> parameters) {
        this.parameters = parameters;
    }
}
