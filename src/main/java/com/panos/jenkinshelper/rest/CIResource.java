package com.panos.jenkinshelper.rest;

import com.panos.jenkinshelper.model.JenkinsBuildHistoryStats;
import com.panos.jenkinshelper.model.JenkinsJob;
import com.panos.jenkinshelper.model.JobParameter;
import com.panos.jenkinshelper.rest.model.JenkinsBuildEntryView;
import com.panos.jenkinshelper.rest.model.JobParameterView;
import com.panos.jenkinshelper.rest.model.RunJobView;
import com.panos.jenkinshelper.service.JenkinsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api")
@Api(value = "CI", description = "A restful API for Jenkins")
public class CIResource {

    private final RestMapper restMapper;
    private final JenkinsService jenkinsService;

    @Autowired
    public CIResource(RestMapper restMapper, JenkinsService jenkinsService) {
        this.restMapper = restMapper;
        this.jenkinsService = jenkinsService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/jobs/{jobName}/build")
    @ApiOperation(value = "Run a job in CI")
    public ResponseEntity<RunJobView> runJob(@PathVariable("jobName") String jobName, @RequestBody List<JobParameterView> parameters) {
        return new ResponseEntity<>(
                new RunJobView(jenkinsService
                        .runJob(jobName, parameters.stream().map(view -> restMapper.map(view, JobParameter.class)).collect(toList())
                        )),
                HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/jobs")
    @ApiOperation(value = "Get jobs from CI")
    public ResponseEntity<List<JenkinsJob>> getJobs() {
        return new ResponseEntity<>(
                jenkinsService.getJobs().stream().limit(10).collect(toList()), HttpStatus.OK
        );
    }

    @RequestMapping(method = RequestMethod.GET, value = "/jobs/{jobName}/last-build")
    @ApiOperation(value = "Get last build of a job in CI")
    public ResponseEntity<JenkinsBuildEntryView> getLastBuild(@PathVariable("jobName") String jobName) {
        return new ResponseEntity<>(
                JenkinsBuildEntryView.fromEntry(jenkinsService.getLastBuild(jobName)), HttpStatus.OK
        );
    }

    @RequestMapping(method = RequestMethod.GET, value = "/jobs/{jobName}/history/stats")
    @ApiOperation(value = "Get stats history of a build in CI")
    public ResponseEntity<JenkinsBuildHistoryStats> getJobHistoryStats(@PathVariable("jobName") String jobName) {
        return new ResponseEntity<>(
                jenkinsService.getJobHistoryStats(jobName), HttpStatus.OK
        );
    }

    @RequestMapping(method = RequestMethod.GET, value = "/jobs/{jobName}/history")
    @ApiOperation(value = "Get history of a build in CI")
    public ResponseEntity<List<JenkinsBuildEntryView>> getJobHistory(@PathVariable("jobName") String jobName) {
        return new ResponseEntity<>(
                jenkinsService.getJobHistory(jobName).stream()
                        .map(JenkinsBuildEntryView::fromEntry).collect(toList()), HttpStatus.OK
        );
    }

}
