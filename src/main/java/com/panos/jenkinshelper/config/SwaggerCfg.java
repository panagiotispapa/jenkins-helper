package com.panos.jenkinshelper.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.concurrent.Callable;

import static java.util.Collections.emptyList;
import static springfox.documentation.builders.PathSelectors.regex;

@ConditionalOnProperty(name = "swagger.enabled", havingValue = "true")
@Configuration
@EnableSwagger2
public class SwaggerCfg {

    @Bean
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("jenkins-helper")
                .apiInfo(new ApiInfo("jenkins-helper",
                        "Jenkins Helper", "", "",
                        new Contact("", "", ""),
                        "", "", emptyList()))
                .genericModelSubstitutes(Callable.class, ResponseEntity.class)
                .select()
                .paths(regex("/api/.*")) // and by paths
                .build();
    }

}
