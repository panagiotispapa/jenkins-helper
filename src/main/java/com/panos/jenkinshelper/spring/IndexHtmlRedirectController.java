package com.panos.jenkinshelper.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexHtmlRedirectController {

    @RequestMapping("/")
    public String redirectToSwagger() {
        return "redirect:swagger-ui.html";
    }

    @RequestMapping("/testerOld")
    public String redirectToTester() {
        return "redirect:/testerOld/jenkinsJobs.html";
    }

    @RequestMapping("/tester")
    public String redirectToTester2() {
        return "redirect:/tester/index.html";
    }

}
