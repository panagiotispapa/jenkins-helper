package com.panos.jenkinshelper.spring;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class CustomWebMvcConfigurer implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/testerOld/**").addResourceLocations("classpath:/tester/");
        registry.addResourceHandler("/tester/**").addResourceLocations("classpath:/tester2/");
    }

    //    for local dev
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**").allowedMethods("OPTIONS", "PUT", "POST", "GET", "DELETE")
                .allowedOrigins("http://localhost:3000");
    }

}