package com.panos.jenkinshelper.spring;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("jenkins")
public class JenkinsConfiguration {


    private String jobsUrl;
    private String jobHistoryUrl;
    private String buildJobUrl;
    private String jobItemHistoryUrl;
    private String jobLastBuildUrl;
    private String queuedJobUrl;

    public String getJobsUrl() {
        return jobsUrl;
    }

    public void setJobsUrl(String jobsUrl) {
        this.jobsUrl = jobsUrl;
    }

    public String getJobHistoryUrl() {
        return jobHistoryUrl;
    }

    public void setJobHistoryUrl(String jobHistoryUrl) {
        this.jobHistoryUrl = jobHistoryUrl;
    }

    public String getBuildJobUrl() {
        return buildJobUrl;
    }

    public void setBuildJobUrl(String buildJobUrl) {
        this.buildJobUrl = buildJobUrl;
    }

    public String getJobItemHistoryUrl() {
        return jobItemHistoryUrl;
    }

    public void setJobItemHistoryUrl(String jobItemHistoryUrl) {
        this.jobItemHistoryUrl = jobItemHistoryUrl;
    }

    public String getJobLastBuildUrl() {
        return jobLastBuildUrl;
    }

    public void setJobLastBuildUrl(String jobLastBuildUrl) {
        this.jobLastBuildUrl = jobLastBuildUrl;
    }

    public String getQueuedJobUrl() {
        return queuedJobUrl;
    }

    public void setQueuedJobUrl(String queuedJobUrl) {
        this.queuedJobUrl = queuedJobUrl;
    }
}
