package com.panos.jenkinshelper;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

public class Utils {

    public static <T> Predicate<T> p(Predicate<T> p) {
        return p;
    }

    public static <T, R> Function<T, R> f(Function<T, R> f) {
        return f;
    }

    public static <T> Comparator<T> c(Comparator<T> c) {
        return c;
    }

}
