package com.panos.jenkinshelper.repository;

import com.google.gson.Gson;
import com.panos.jenkinshelper.model.*;
import com.panos.jenkinshelper.spring.JenkinsConfiguration;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Repository
public class JenkinsRepository {

    private static final Pattern BUILD_NUMBER = Pattern.compile("^.*?/(\\d*)/?$");

    private final CloseableHttpClient ciHttpClient;
    private final RestTemplate ciRestTemplate;
    private final JenkinsConfiguration jenkinsConfiguration;

    private static final Function<CloseableHttpResponse, String> stringParser = resp -> {
        try {
            return EntityUtils.toString(resp.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    };

    @Autowired
    public JenkinsRepository(
            CloseableHttpClient ciHttpClient,
            RestTemplate ciRestTemplate,
            JenkinsConfiguration jenkinsConfiguration) {
        this.ciHttpClient = ciHttpClient;
        this.ciRestTemplate = ciRestTemplate;
        this.jenkinsConfiguration = jenkinsConfiguration;
    }

    public String runJob(String jobName, List<JobParameter> parameters) {

        final String queueId = queueJob(jobName, parameters);
        JenkinsQueuedItem queuedItem = fetchQueuedItem(queueId);
        return ofNullable(queuedItem.getExecutable())
                .map(JenkinsQueuedItem.Executable::getUrl)
                .orElseGet(() -> queuedItem.getTask().getUrl());

    }

    public List<JenkinsBuildEntry> getJobHistory(String jobName) {
        HttpGet get = new HttpGet(jenkinsConfiguration.getJobHistoryUrl().replace("{jobName}", jobName));

        Gson gson = new Gson();

        return gson.fromJson(callJenkins(get, stringParser), JenkinsJobHistory.class).getBuilds();
    }

    public String queueJob(String jobName, List<JobParameter> parameters) {
        return postJenkins(jenkinsConfiguration.getBuildJobUrl().replace("{jobName}", jobName),
                parameters.stream().map(p -> new BasicNameValuePair(p.getName(), p.getValue())).collect(toList())
        );
    }

    public List<JenkinsJob> getJobs() {
        HttpGet get = new HttpGet(jenkinsConfiguration.getJobsUrl());
        Gson gson = new Gson();

        return gson.fromJson(callJenkins(get, stringParser), JenkinsJobsResponse.class).getJobs();
    }

    public Optional<JenkinsBuildEntry> getLastBuild(String jobName) {

        HttpGet get = new HttpGet(jenkinsConfiguration.getJobLastBuildUrl().replace("{jobName}", jobName));

        Gson gson = new Gson();

        JenkinsBuildEntry response = gson.fromJson(callJenkins(get, stringParser), JenkinsBuildEntry.class);
        response.setName(jobName);
        return Optional.of(response);
    }

    public JenkinsBuildHistoryStats getJobHistoryStats(String jobName) {
        JenkinsJobHistorySimple history = ciRestTemplate.exchange(jenkinsConfiguration.getJobHistoryUrl(), HttpMethod.GET, new HttpEntity<>(null), JenkinsJobHistorySimple.class, jobName).getBody();
        return new JenkinsBuildHistoryStats(history.getBuilds());
    }

    private JenkinsQueuedItem fetchQueuedItem(String queueId) {

        HttpGet get = new HttpGet(jenkinsConfiguration.getQueuedJobUrl().replace("{queueId}", queueId));

        Gson gson = new Gson();
        return gson.fromJson(callJenkins(get, stringParser), JenkinsQueuedItem.class);
    }

    private String postJenkins(final String url, final List<NameValuePair> data) {

        final HttpPost request = new HttpPost(url);
        if (!data.isEmpty()) {
            request.setEntity(new UrlEncodedFormEntity(data, StandardCharsets.UTF_8));
        }
        return callJenkins(request, o -> extractBuildNumber(o.getFirstHeader(HttpHeaders.LOCATION).getValue())).get();
    }

    private Optional<String> extractBuildNumber(final String url) {
        final Matcher m = BUILD_NUMBER.matcher(url);
        return m.matches() ? Optional.of(m.group(1)) : Optional.empty();
    }

    private <T> T callJenkins(final HttpUriRequest request, final Function<CloseableHttpResponse, T> mapper) {

        try {
            CloseableHttpResponse closeableHttpResponse = ciHttpClient.execute(request, buildContext());
            return mapper.apply(closeableHttpResponse);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static BasicHttpContext buildContext() {
        BasicScheme basicAuth = new BasicScheme();
        BasicHttpContext context = new BasicHttpContext();
        context.setAttribute("preemptive-auth", basicAuth);
        return context;
    }

}
