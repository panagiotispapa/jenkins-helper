## jenkins helper ##

This app provides a simple RestFul Api for Jenkins and a simple Bootstrap frontend to facilitate viewing the jobs' previous builds.

### Requirements ###

Docker version 18.03.1-ce and above
docker-compose version 1.21.1 and above

### Build ###

./scripts/./build_and_create_image.sh

### Execute ###

JENKINS_USER=user JENKINS_API_KEY=key ./scripts/./up.sh

### Stop ###

./scripts/./down.sh

### See logs ###

./scripts/./logs.sh

### Get a sh console in the container ###

./scripts/./in.sh

### Restful API ###

http://localhost:8090/

### Simple Web UI ###

Open url:

http://localhost:8090/tester/
