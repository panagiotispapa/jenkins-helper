require 'sinatra'

  before do
    content_type 'application/json'
  end


	get '/frank-says' do
	  'Put this in your pipe & smoke it!'
	end


	get '/api/json' do
	  File.read("jobs.json")
	end


	get '/test/:date' do
		# "Hello #{params['date']}!"
	  File.read("#{params['date']}.json")
	end
