#!/usr/bin/env bash

function check_health {
    container_id=$(docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} ps -q $1)
    [[ ! -z  ${container_id} ]] && status=$(docker inspect --format "{{json .State.Health.Status }}" "$container_id") && echo "$1 is ${status//\"}"
}

function check_health_of_services {
    check_health jenkins-service
#    check_health next-service
}

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export DOCKER_COMPOSE_FILE="${PROJECT_DIR}/docker-compose.yml"

check_health_of_services;