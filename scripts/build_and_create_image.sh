#!/usr/bin/env bash

function build {
    echo "Building ${SERVICE}"
    [ -z "$BUILD_TEST_CLIENT" ] && ask_user_for_building_tester_client
    ${WORKSPACE}/scripts/./build.sh || exit 1
    docker build -t ${SERVICE} ${WORKSPACE}
}

function ask_user_for_building_if_image_exists {
    echo "Image already exists for version $IMAGE_TAG. Do you want to build again?"
    select yn in "yes" "no"; do
        case $yn in
            yes ) export BUILD_IF_IMAGE_EXISTS='true'; break;;
            no ) export BUILD_IF_IMAGE_EXISTS='false'; break;;
        esac
    done
}

function ask_user_for_building_tester_client {
    echo "Do you want to include tester client?"
    select yn in "yes" "no"; do
        case $yn in
            yes ) export BUILD_TEST_CLIENT='true'; break;;
            no ) export BUILD_TEST_CLIENT='false'; break;;
        esac
    done
}

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}

export service_name=jenkins-service
export BUILD_TEST_CLIENT=true

export GIT_TAG=$(git tag -l --points-at HEAD)
export COMMIT_HASH=$(git rev-parse HEAD | cut -c1-11)
export IMAGE_TAG=${IMAGE_TAG:-${GIT_TAG:-"${COMMIT_HASH}"}}
export SERVICE="jenkins/${service_name}:${IMAGE_TAG}"

echo "SERVICE: $SERVICE"

images_found=$(docker images ${SERVICE} | wc -l)

if [ ${images_found} -gt 1 ]; then
    [ -z "$BUILD_IF_IMAGE_EXISTS" ] && ask_user_for_building_if_image_exists
    if [ ${BUILD_IF_IMAGE_EXISTS} = 'true' ]; then
        build
    else
        echo ""
    fi
else
    build
fi