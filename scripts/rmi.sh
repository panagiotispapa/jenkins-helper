#!/usr/bin/env bash

function remove_images {
        echo "Image $1"
        docker images -q "jenkins/$1"  | xargs docker rmi -f
}

IMAGES=(jenkins-service)

for i in ${IMAGES[*]}; do
     remove_images ${i}
done
