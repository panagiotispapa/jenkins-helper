#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

#export DOCKER_FILE=${PROJECT_DIR}/docker-compose.yml

docker run --rm -it --name dcv -v ${PROJECT_DIR}:/input pmsipilot/docker-compose-viz render -m image docker-compose.yml