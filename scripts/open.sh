#!/usr/bin/env bash

function open_page {
    unameOut="$(uname -s)"
    if [[ ${unameOut} = 'Linux' ]]; then
        xdg-open $1
    elif [[ ${unameOut} = 'Darwin' ]]; then
        open $1
    else
        echo $1
    fi
}

function open_pages {
    url="http://localhost:$1"
    open_page "${url}/"
    open_page "${url}/tester/"
}

open_pages 8090
