#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}

echo ${WORKSPACE}

docker-compose -f ${WORKSPACE}/docker-compose-build.yml up clean