#!/usr/bin/env bash
# sh merge_master_to_branch.sh
# Don't forget the git push at the end

current_branch=$(git rev-parse --abbrev-ref HEAD)

git checkout master
git pull
git checkout ${current_branch}
git pull origin ${current_branch}
echo 'Merging master to branch'
git merge master
echo 'Merge done. Status:'
git status