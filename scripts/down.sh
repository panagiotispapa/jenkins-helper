#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}

docker-compose --log-level ERROR -f ${WORKSPACE}/docker-compose.yml down -v
docker-compose --log-level ERROR -f ${WORKSPACE}/docker-compose-build.yml down -v
