#!/usr/bin/env bash
# sh merge_branch_to_master.sh "This is the commit message"
# Don't forget the git push at the end


function check_if_branch_is_behind_master {
    echo "in check for $1"
    git checkout master
    git pull
    git checkout $1
    git pull origin $1
    commits_behind_master_cnt=$(git cherry $1 master | wc -l)

#    echo "commits behind $commits_behind_master_cnt"

    if [[ ${commits_behind_master_cnt} -gt 0 ]]; then
        echo "You are behind master by $commits_behind_master_cnt commits"
        exit 1
    fi

}

function merge_branch_to_master {
    echo "Merging $1 with message $2"

    git checkout master
    git merge $1 --ff-only --squash
    git commit -m "$2"
    git status
    git log -4

  }

function check_commit_message {
    if [[ -z "${1// }" ]]; then
        echo "No commit message provided"
        exit 1
    fi
}

check_commit_message $1

current_branch=$(git rev-parse --abbrev-ref HEAD)
echo ${current_branch}

check_if_branch_is_behind_master ${current_branch}
echo "commit message $1"

merge_branch_to_master ${current_branch} "$1"