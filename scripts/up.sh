#!/usr/bin/env bash

function wait_until_service_is_up {
    container_id=$1
    echo "Waiting for $1 with id $container_id"
    while [ $(docker inspect --format "{{json .State.Health.Status }}" "$container_id") != "\"healthy\"" ];
        do printf ".";
        sleep 1;
    done

    echo ""
}

function start_service_and_wait {
    docker-compose --log-level ERROR -f ${DOCKER_FILE} up -d $1
    wait_until_service_is_up $1
}

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

hash=$(git rev-parse HEAD | cut -c1-11)

: ${JENKINS_USER?"Need to set JENKINS_USER"}
: ${JENKINS_API_KEY?"Need to set JENKINS_API_KEY"}

export TAG=${hash}
export JENKINS_USER=${JENKINS_USER}
export JENKINS_API_KEY=${JENKINS_API_KEY}
export DOCKER_FILE=${PROJECT_DIR}/docker-compose.yml
export BUILD_IF_IMAGE_EXISTS='false'

${PROJECT_DIR}/scripts/./build_and_create_image.sh

start_service_and_wait jenkins-service
