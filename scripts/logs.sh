#!/usr/bin/env bash

function logs_for_service {
    container_id=$1
    docker logs -f ${container_id}
}

logs_for_service jenkins-service


