#!/usr/bin/env bash

function cleanup {
    docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} down -v
}

function create_volume {
    docker volume ls -q -f "name=$1" | grep -q . || docker volume create --name=$1
}

function build_tester_client {
    if [[ ${BUILD_TEST_CLIENT} = 'true' ]]; then
        echo "Building tester client"
        ${WORKSPACE}/client/jenkins-app/scripts/./build_node_image.sh || exit 1
        ${WORKSPACE}/client/jenkins-app/scripts/./create_image.sh || exit 1
    else
        echo "Not Building tester client"
    fi
}

function copy_react_tester_assets {
    rm -rf ${WORKSPACE}/target/BOOT-INF;
    mkdir -p ${WORKSPACE}/target/BOOT-INF/classes/tester2;
    docker run --name temp-jenkins-client-build-container jenkins/jenkins-app:latest /bin/true;
    docker cp temp-jenkins-client-build-container:/usr/share/nginx/html/. ${WORKSPACE}/target/BOOT-INF/classes/tester2;
    docker rm temp-jenkins-client-build-container;
    (cd ${WORKSPACE}/target; jar uf jenkins-helper.jar BOOT-INF)
}

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}

export BUILD_TEST_CLIENT=${BUILD_TEST_CLIENT:-'true'}
export DOCKER_COMPOSE_FILE=${WORKSPACE}/docker-compose-build.yml

echo ${WORKSPACE}

trap cleanup EXIT

build_tester_client

create_volume jenkins-helper-maven-cache

docker-compose -f ${DOCKER_COMPOSE_FILE} up --exit-code-from jenkins-helper-build jenkins-helper-build || exit 1

copy_react_tester_assets