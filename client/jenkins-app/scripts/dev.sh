#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

export SERVER_PORT=8090;

docker run -it --rm --name jenkins-app-dev -p 3000:3000 -v "$PROJECT_DIR":/usr/src/app -w /usr/src/app -e REACT_APP_SERVER_URL="http://localhost:${SERVER_PORT}" jenkins/node_react:8 /bin/bash