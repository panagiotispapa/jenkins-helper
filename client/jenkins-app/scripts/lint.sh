#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

docker run --rm -v "$PROJECT_DIR":/usr/src/app -w /usr/src/app jenkins/node:8 npm run lint

