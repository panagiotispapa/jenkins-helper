#!/usr/bin/env bash

function remove_images {
        echo "Image $1"
        docker images -q "jenkins/$1"  | xargs docker rmi -f
}

remove_images jenkins-app
#remove_images node_react