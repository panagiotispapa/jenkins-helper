#!/usr/bin/env bash

IMAGES=(jenkins-app node_react)

for i in ${IMAGES[*]}; do
    docker images jenkins/${i}
done

#export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
#docker-compose --log-level ERROR -f ${PROJECT_DIR}/docker-compose.yml images
