#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

docker build -t jenkins/node_react:8 -f ${PROJECT_DIR}/Dockerfile_node ${PROJECT_DIR}