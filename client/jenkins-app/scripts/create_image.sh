#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml

sh ${PROJECT_DIR}/scripts/build_node_image.sh

docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} build jenkins-app
#docker build -t jenkins/jenkins-app ${PROJECT_DIR}