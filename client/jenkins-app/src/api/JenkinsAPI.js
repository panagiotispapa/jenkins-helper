import API from './api'
import {jenkinsJobErrorHandler} from "../unit/utility";

class JenkinsAPI {

    static getJobs() {
        return API.get("/api/jobs");
    }

    static getJobHistory(jobName) {
        return API.get(`/api/jobs/${jobName}/history`);
    }

    static startJob(jobName, params, callback) {
        API.post(`/api/jobs/${jobName}/build`, params).then(callback).catch(jenkinsJobErrorHandler)
    }

}

export default JenkinsAPI