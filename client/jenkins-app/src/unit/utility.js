export const updateObject = (oldObject, updatedProperties) => ({
    ...oldObject,
    ...updatedProperties
});

export const chunk = (array, chunkSize) => {
    let R = [];
    for (let i = 0; i < array.length; i += chunkSize)
        R.push(array.slice(i, i + chunkSize));
    return R;

};

export const and = (predicates) => (e) => predicates.every(p => p(e));

export const buildAction = (type, data) => ({type, data});
export const stateForKey = (key) => (state) => ({...state.get(key)});

export const stateForKeys = (keys) => (state) => ({...keys.map(k => state.get(k)).reduceRight((accumulatedState, current) => updateObject(accumulatedState, current), {})});

export const loading = (oldObject) => updateObject(oldObject, {loading: true});
export const loaded = (oldObject) => updateObject(oldObject, {loading: false});
export const show = (oldObject) => updateObject(oldObject, {show: true});
export const hide = (oldObject) => updateObject(oldObject, {show: false});

export const toArray = (elements) => elements instanceof Array ? elements : [elements];


export const jenkinsJobErrorHandler = error => {
    alert("An error has occurred while trying to start the job. Please check Jenkins if the job has started.");
    console.log(error);
};