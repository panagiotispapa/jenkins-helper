import React, {Component} from 'react';
import {HashRouter, Redirect, Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import './App.css';
import JobsPage from "./pages/JobsPage";
import JobPage from "./pages/JobPage";

class App extends Component {
    render() {
        return (

            <HashRouter>
                <div className="content">

                    <Switch>

                        <Route path="/jobs/:job" component={JobPage}/>
                        <Route path="/jobs" component={JobsPage}/>
                        <Route exact path="/" component={JobsPage}/>
                        <Redirect to="/"/>
                    </Switch>

                </div>

            </HashRouter>
        );
    }
}

export default connect()(App);
//export default App;
