import React, {Component} from 'react';
import JenkinsBuildHistory from "../components/JenkinsBuildHistory";
import JenkinsAPI from "../api/JenkinsAPI";
import {loaded} from "../unit/utility";
import ReRunBuildServiceModal from "../components/modals/ReRunBuildServiceModal";
import OpenJenkinsModal from "../components/modals/OpenJenkinsModal";

class JobPage extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            job: null,
            loading: true,
            builds: [],
        }
    }

    componentWillMount() {
        this.getJobHistory(this.props);
        this.refreshBuilds();
    }

    componentWillUnmount() {
        clearTimeout(this.jobsTimeout);
    };

    refreshBuilds() {
        this.jobsTimeout = setTimeout(() => {
            this.getJobHistory(this.props);
            this.refreshBuilds();
        }, 5000);
    }

    getJobHistory(props) {
        const job = props.match.params.job;
        this.setState({job});
        console.log("job: " + job);
        JenkinsAPI.getJobHistory(job).then(resp => this.setState(loaded({builds: resp.data})))
    }

    render() {
        return (
            <React.Fragment>
                <ReRunBuildServiceModal/>
                <OpenJenkinsModal/>
                <JenkinsBuildHistory {...this.state}/>
            </React.Fragment>
        )
    }
}


export default JobPage;