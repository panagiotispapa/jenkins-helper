import React, {Component} from 'react';
import Jobs from "../components/Jobs";
import JenkinsAPI from "../api/JenkinsAPI";
import {loaded} from "../unit/utility";

class JobsPage extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            loading: true,
            jobs: [],
        }
    }

    componentDidMount() {
        this.getJobs();
        this.refreshJobs();
    }

    componentWillUnmount() {
        clearTimeout(this.jobsTimeout);
    };

    refreshJobs() {
        this.jobsTimeout = setTimeout(() => {
            this.getJobs();
            this.refreshJobs();
        }, 5000);
    }

    getJobs() {
        JenkinsAPI.getJobs().then(resp => this.setState(loaded({jobs:resp.data})))
    }

    render() {
        return (
            <React.Fragment>
                <Jobs {...this.state}/>
            </React.Fragment>
        )
    }
}


export default JobsPage;