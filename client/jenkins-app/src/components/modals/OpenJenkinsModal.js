import React, {Component} from 'react';
import {Button, Modal} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import * as actions from "../../store/actions";
import {stateForKey} from "../../unit/utility";
import ExternalLink from "../common/ExternalLink";

class OpenJenkinsModal extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleClose = this.handleClose.bind(this);
    }

    handleClose() {
        this.hide();
    }

    hide() {
        this.props.onHideJenkinsJobModal();
    }

    render() {
        return (
            <Modal show={this.props.show || false}>
                <Modal.Header>
                    <Modal.Title>Build has started</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    Build has started.
                </Modal.Body>

                <Modal.Footer>
                    <ExternalLink href={this.props.url} text={"Open"}/>
                    &nbsp;&nbsp;
                    <Button variant={"secondary"} onClick={this.handleClose}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

OpenJenkinsModal.propTypes = {
    url: PropTypes.string,
    show: PropTypes.bool
};

const mapDispatchToProps = dispatch => ({
    onHideJenkinsJobModal: () => dispatch(actions.hideJenkinsJobModal()),
});

export default connect(stateForKey('jobData'), mapDispatchToProps)(OpenJenkinsModal);