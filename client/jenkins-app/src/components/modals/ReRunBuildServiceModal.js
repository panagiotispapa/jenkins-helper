import React, {Component} from 'react';
import {Button, Modal} from 'react-bootstrap';
import PropTypes from 'prop-types';
import TextGroup from "./TextGroup";
import {connect} from 'react-redux'
import * as actions from "../../store/actions";
import {stateForKey} from "../../unit/utility";

class ReRunBuildServiceModal extends Component {

    constructor(props, context) {

        super(props, context);

        this.handleEnter = this.handleEnter.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleBuild = this.handleBuild.bind(this);
    }


    handleEnter() {

    }

    handleClose() {
        this.props.onHideModal();

    }

    handleBuild() {
        this.props.onReRunJenkinsJob(this.props.jobName, this.props.params);
    }

    render() {
        return (
            <div>
                <Modal show={this.props.show} onEnter={this.handleEnter}>
                    <Modal.Header>
                        <Modal.Title>Re run job</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form>
                            <TextGroup id={"build-service-id"} label="Job:" value={this.props.jobName}
                                       readOnly={"readOnly"} type="text"/>
                        </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant={"secondary"} onClick={this.handleClose}>Close</Button>
                        <Button variant="primary" onClick={this.handleBuild}>Build</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

ReRunBuildServiceModal.propTypes = {
    show: PropTypes.bool,
    jobName: PropTypes.string,
    params: PropTypes.array,
};

const mapDispatchToProps = dispatch => ({
    onHideModal: () => dispatch(actions.hideReRunJobModal()),
    onReRunJenkinsJob: (jobName, params) => dispatch(actions.reRunJenkinsJob(jobName, params)),
});


export default connect(stateForKey('reRunBuildData'), mapDispatchToProps)(ReRunBuildServiceModal);