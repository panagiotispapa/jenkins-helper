import React, {Component} from 'react';
import {Form} from 'react-bootstrap';
import PropTypes from "prop-types";

class TextGroup extends Component {
    render() {
        const props = this.props;
        return (
            <Form.Group controlId={props.id}>
                <Form.Label>{props.label}</Form.Label>
                <Form.Control {...props}/>
            </Form.Group>
        )
    }
}

TextGroup.propTypes = {
    inputRef: PropTypes.func,
    readOnly: PropTypes.string,
    value: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string
};

export default TextGroup;