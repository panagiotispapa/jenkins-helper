import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Col} from "react-bootstrap";
import {toArray} from "../../unit/utility";

class Cols extends Component {
    render() {
        let {data,name, ...colProps} = this.props;
        return (
            (this.props.data || toArray(this.props.children)).map((d, index) => <Col {...colProps} key={`${name || "Cols"}-${index}`}>{d}</Col>)
        );
    }
}

Cols.propTypes = {
    data: PropTypes.array,
    name: PropTypes.string,
};

export default Cols