import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Row} from "react-bootstrap";
import {toArray} from "../../unit/utility";

class Rows extends Component {
    render() {
        return (
            (this.props.data || toArray(this.props.children)).map(d => <Row {...this.props}>{d}</Row>)
        );
    }
}

Rows.propTypes = {
    data: PropTypes.array,
};


export default Rows