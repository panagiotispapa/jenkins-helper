import PropTypes from "prop-types";
import React, {Component} from 'react';
import {Col, Container, Row} from "react-bootstrap";

class VerticalGrid extends Component {
    render() {
        const name = this.props.name || "vertical_grid";
        let children = this.props.children instanceof Array ? this.props.children : [this.props.children];
        return (
            <Container fluid={this.props.fluid || true}>
                {[].concat(children).map(
                    (c, index) =>
                        <Row key={`${name}_row_${index}`}>
                            <Col {...this.props} key={`${name}_col_${index}`}>{c}</Col>
                        </Row>)
                }
            </Container>
        );
    }
}

VerticalGrid.propTypes = {
    fluid: PropTypes.bool,
    name: PropTypes.string
};

export default VerticalGrid;