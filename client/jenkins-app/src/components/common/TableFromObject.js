import React, {Component} from 'react';
import Table from "./Table";
import PropTypes from "prop-types";

class TableFromObject extends Component {
    render() {

        const keys = this.props.obj ? Object.keys(this.props.obj) : null;

        const view = this.props.obj ?
            <Table //header={["keys", "values"]}
                   body={keys.map(k => [<strong>{k}</strong>, this.props.obj[k]])}
                   striped={true}
            />
            : null;

        return (
            view
        );
    }
}

TableFromObject.propTypes = {
    obj: PropTypes.object
};

export default TableFromObject