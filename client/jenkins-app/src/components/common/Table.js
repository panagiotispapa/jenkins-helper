import React, {Component} from 'react';
import { Table as BSTable}  from 'react-bootstrap';
import PropTypes from 'prop-types';

class Table extends Component {
    render() {
        let body = this.props.body || [];
        return (
            body.length > 0 ?
            <BSTable responsive={true} condensed={this.props.condensed}
                     striped={this.props.striped}
                     bordered={this.props.bordered}
            >
                <TableHeader data={this.props.header || []}/>
                <TableBody data={this.props.body || []}/>
            </BSTable> : null
        )
    }
}

class TableHeader extends Component {

    render() {
        const headers = this.props.data.map((row, index) => (<th key={index}>{row}</th>));
        return (
            <thead>
            <tr>
                {headers}
            </tr>
            </thead>
        );
    }
}

TableHeader.propTypes = {
    data: PropTypes.array.isRequired
};

class TableBody extends Component {
    render() {

        const tds = columns => columns.map((r, i) => (<td key={i}>{r}</td>));

        const trs = this.props.data.map((r, i) => (<tr key={i}>{tds(r)}</tr>));

        return (
             <tbody>
             {trs}
             </tbody>
         );
     }
}

TableBody.propTypes = {
    data: PropTypes.array.isRequired
};

Table.propTypes = {
    header: PropTypes.array,
    body: PropTypes.array,
    condensed: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};


export default Table