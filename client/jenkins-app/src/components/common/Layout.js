import React, {Component} from 'react';
import './Layout.css';
import PropTypes from "prop-types";
import VerticalGrid from "./VerticalGrid";

class Layout extends Component {
    render() {
        return (
            <main {...(this.props.withNavBar && {className: "Content"})}>
                <VerticalGrid>
                    {this.props.children}
                </VerticalGrid>
            </main>
        )
    }
}

Layout.propTypes = {
    withNavBar: PropTypes.bool,
};

export default Layout