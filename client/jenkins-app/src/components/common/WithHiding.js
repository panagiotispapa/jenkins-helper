import React from 'react';

const withHiding = (WrappedComponent) => {
    const WithHiding = props => (
        props.show ? <WrappedComponent ref={props.forwardedRef} {...props} /> : null
    );

    return React.forwardRef((props, ref) => {
        return <WithHiding {...props} forwardedRef={ref}/>
    });
};

export default withHiding;