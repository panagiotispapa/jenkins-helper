import React, {Component} from 'react';
import {Col, Container, Row} from 'react-bootstrap';
import PropTypes from 'prop-types';

class CustomGrid extends Component {
    render() {

        const name = this.props.name || "grid";

        return (
            <Container fluid={this.props.fluid || true}>
                {this.props.children.map((row, index) => <Row key={name + "_" + index}>
                    {row.map((e, indexEl) => {

                            const md = this.props.mds ? this.props.mds[indexEl] : (this.props.md);
                            const xs = this.props.xss ? this.props.xss[indexEl] : (this.props.xs);

                            return <Col
                                {...(md && {md: md})}
                                {...(xs && {xs: xs})}
                                key={name + "_" + index + "_" + indexEl}>
                                {e}
                            </Col>
                        }
                    )}

                </Row>)}

            </Container>
        )
    }
}

CustomGrid.propTypes = {
    md: PropTypes.number,
    sm: PropTypes.number,
    xs: PropTypes.number,
    mds: PropTypes.array,
    xss: PropTypes.array,
    name: PropTypes.string,
    fluid: PropTypes.bool
};


export default CustomGrid