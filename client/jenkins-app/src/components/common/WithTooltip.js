import React, {Component} from 'react';
import PropTypes from "prop-types";
import {OverlayTrigger, Tooltip} from "react-bootstrap";

class WithTooltip extends Component {
    render() {

        const tooltip = this.props.tooltip;
        return (
            tooltip ?

                <OverlayTrigger
                    key={'tooltip-id'}
                    placement={'top'}
                    overlay={
                        <Tooltip id={`tooltip-sss`}>
                            {tooltip}
                        </Tooltip>
                    }
                >
                    {this.props.children}
                </OverlayTrigger> : this.props.children

        );
    }
}

WithTooltip.propTypes = {
    tooltip: PropTypes.string,

};


export default WithTooltip;