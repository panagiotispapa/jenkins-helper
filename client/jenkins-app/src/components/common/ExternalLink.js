import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Button} from "react-bootstrap";

class ExternalLink extends Component {
    render() {
        return (<Button variant={"link"} href={this.props.href} target={"_blank"}>{this.props.text || this.props.children} </Button>);
    }
}

ExternalLink.propTypes = {
    href: PropTypes.string,
    text: PropTypes.string,
};

export default ExternalLink;