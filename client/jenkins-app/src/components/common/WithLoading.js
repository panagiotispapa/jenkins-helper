import React from 'react';
import Spinner from "./spinner/Spinner";

const withLoading = (WrappedComponent) => {
    const WithLoading = props => (
        <React.Fragment>
            {props.loading ? <Spinner/> : <WrappedComponent ref={props.forwardedRef} {...props} />}
        </React.Fragment>

    );

    return React.forwardRef((props, ref) => (<WithLoading {...props} forwardedRef={ref}/>));
};


export default withLoading;