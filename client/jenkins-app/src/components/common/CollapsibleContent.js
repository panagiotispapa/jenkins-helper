import React, {Component} from 'react';
import {Button, Collapse} from "react-bootstrap";
import PropTypes from "prop-types";

class CollapsibleContent extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            open: false,
        };
    }

    render() {

        const {open} = this.state;

        return (
            <React.Fragment>
                <Button
                    onClick={() => this.setState({open: !open})}
                    aria-controls="example-collapse-text"
                    aria-expanded={open}
                >
                    {this.props.label}
                </Button>
                <Collapse in={this.state.open}>
                    <div id={`collapse-text-${this.props.id}`}>
                        {this.props.children}
                    </div>
                </Collapse>
            </React.Fragment>
        );
    }
}

CollapsibleContent.propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,

};

export default CollapsibleContent;