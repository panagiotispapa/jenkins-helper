import React, {Component} from 'react';
import PropTypes from "prop-types";
import Table from "./Table";

class TableFromKeys extends Component {
    render() {
        const keys = this.props.keys;
        const elements = this.props.elements;
        return (
            <Table header={keys} body={elements.map(e => keys.map(k => e[k]))} {...this.props}/>
        );
    }
}

TableFromKeys.propTypes = {
    elements: PropTypes.array.isRequired,
    keys: PropTypes.array.isRequired,
    condensed: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};

export default TableFromKeys;