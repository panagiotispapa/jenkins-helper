import React, {Component} from 'react';
import PropTypes from "prop-types";
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";

class RadioGroup extends Component {
    render() {
        return (
            <ToggleButtonGroup
                type="radio"
                name="options" defaultValue={this.props.defaultValue}
            >
                {this.props.values.map(s => <ToggleButton onClick={() => this.props.handleChange(s.id)} value={s.id}>{s.value}</ToggleButton>)}
            </ToggleButtonGroup>
        )
    }
}

RadioGroup.propTypes = {
    defaultValue: PropTypes.string,
    values: PropTypes.arrayOf(PropTypes.shape(
        {
            id:PropTypes.string,
            value:PropTypes.string,
        }
    )),
    handleChange: PropTypes.func,
};

export default RadioGroup