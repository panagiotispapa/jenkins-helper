import React, {Component} from 'react';
import Table from "./Table";
import PropTypes from 'prop-types';

class VerticalTable extends Component {

    render() {
        return (
            <Table header={[]} body={(this.props.data || this.parseChildren()).map(el => [el])} condensed={this.props.condensed} striped={this.props.striped}/>
        )
    }

    parseChildren() {
        if(this.props.children) {
            return this.props.children instanceof Array ? this.props.children : [this.props.children];
        } else {
            return null;
        }
    }
}

VerticalTable.propTypes = {
    data: PropTypes.array,
    condensed: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};

export default VerticalTable