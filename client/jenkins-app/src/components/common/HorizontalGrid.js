import PropTypes from "prop-types";
import React, {Component} from 'react';
import {Col, Container, Row} from "react-bootstrap";

class HorizontalGrid extends Component {
    render() {
        const name = this.props.name || "horizontal_grid";
        let children = this.props.children instanceof Array ? this.props.children : [this.props.children];
        return (
            <Container fluid={this.props.fluid || true}>
                <Row>
                    {[].concat(children).map((c, index) => {
                        const xs = this.props.xss ? this.props.xss[index] : (this.props.xs);
                        return (
                            <Col
                                {...this.props}
                                {...(xs && {xs: xs})}
                                key={`${name}_col_${index}`}
                            >{c}</Col>);
                    })}
                </Row>
            </Container>
        )
    }
}

HorizontalGrid.propTypes = {
    xss: PropTypes.array,
    fluid: PropTypes.bool,
    name: PropTypes.string
};

export default HorizontalGrid