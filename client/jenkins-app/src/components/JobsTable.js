import React, {Component} from 'react';
import PropTypes from "prop-types";
import withLoading from "./common/WithLoading";
import JobStatus from "./JobStatus";
import Table from "./common/Table";
import {Button} from "react-bootstrap";

class JobsTable extends Component {
    render() {
        return (
            <Table striped={true} header={["#", "Job"]}
                   body={this.props.jobs.map(job => [
                       <JobStatus color={job.color} link={job.url}/>,
                       <Button href={`#/jobs/${job.name}`} variant={"link"}>{job.name}</Button>,
                   ])}
            />
        );
    }
}

JobsTable.propTypes = {
    jobs: PropTypes.array,
    loading: PropTypes.bool
};

export default withLoading(JobsTable)