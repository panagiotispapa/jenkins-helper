import React, {Component} from 'react';
import PropTypes from "prop-types";
import './dots.css'
import ExternalLink from "./common/ExternalLink";

class JobStatus extends Component {
    render() {
        return (
            <React.Fragment>
                <ExternalLink href={this.props.link}>
                    <SuccessfullJob {...this.props}/>
                    <FailedJob {...this.props}/>
                    <NoResultJob {...this.props}/>
                </ExternalLink>
            </React.Fragment>
        );
    }
}

const SuccessfullJob = props => (
    props.color === "blue" ? <span className={"blueDot"}/> : null
);

const FailedJob = props => (
    props.color === "red" ? <span className={"redDot"}/> : null
);

const NoResultJob = props => (
    props.color === "notbuilt" ? <i className="fas fa-external-link-square-alt"/> : null
);

JobStatus.propTypes = {
    color: PropTypes.string,
    link: PropTypes.string,
};

export default JobStatus;