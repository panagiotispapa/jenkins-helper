import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import * as actions from '../store/actions';
import PropTypes from "prop-types";

class ReRunBuildButton extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.onSetReRunBuildData(this.props);
    }

    render() {
        return (
            <Button variant="warning" onClick={this.handleClick} {...(this.props.params.length === 0 && {disabled: true})}>
                re-run
            </Button>
        )
    }

}

ReRunBuildButton.propTypes = {
    jobName: PropTypes.string.isRequired,
    params: PropTypes.array.isRequired,
};

const mapDispatchToProps = dispatch => ({
    onSetReRunBuildData: (serviceId, branch, skipTests) => dispatch(actions.setReRunBuildData(serviceId, branch, skipTests)),
});

export default connect(null, mapDispatchToProps)(ReRunBuildButton);
