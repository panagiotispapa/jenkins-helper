import React, {Component} from 'react';
import PropTypes from "prop-types";
import Table from "./common/Table";
import moment from "moment/moment";
import ReRunBuildButton from "./ReRunBuildButton";
import VerticalGrid from "./common/VerticalGrid";
import ExternalLink from "./common/ExternalLink";
import {Col, Container, Row} from "react-bootstrap";

class JenkinsBuildHistoryTable extends Component {
    render() {
        let job = this.props.job;
        return (
            <Table header={[
                "Id", "Result", "Params", "Actions"
            ]} bordered={true} body={
                this.props.builds.map(build =>
                    [
                        <Link id={build.id} url={build.url}/>,
                        <Result build={build}/>,
                        <Params params={build.parameters}/>,
                        <Actions jobName={job} params={build.parameters}/>,
                    ]
                )
            }/>
        );
    }
}

const Link = props => {
    const {id, url} = props;
    return (
        <ExternalLink href={url} text={id} variant={"outline-info"}/>
    )
};

Link.propTypes = {
    id: PropTypes.string,
    url: PropTypes.string,
};


const Result = props => {
    const {result, by, timestamp} = props.build;
    return (
        <VerticalGrid>
            {result}
            {`by ${by}`}
            {moment(timestamp).fromNow()}
        </VerticalGrid>
    )
};

Result.propTypes = {
    build: PropTypes.object
};

const Params = props => {
    const {params} = props;
    let toRow = elements => (<Row>
        <Col xs={4}>{elements[0]}</Col><Col xs={8}>{elements[1]}</Col>
    </Row>);
    return (params ?
        <Container>
            {params.map(item => [
                item.name,
                `${item.value}`
            ]).map(toRow)}
        </Container> : null
    )
};

Params.propTypes = {
    params: PropTypes.array.isRequired
};

const Actions = props => {
    return (
        <ReRunBuildButton jobName={props.jobName} params={props.params}/>
    )
};

Actions.propTypes = {
    jobName: PropTypes.string,
    params: PropTypes.array
};


JenkinsBuildHistoryTable.propTypes = {
    job: PropTypes.string,
    builds: PropTypes.array
};

export default JenkinsBuildHistoryTable