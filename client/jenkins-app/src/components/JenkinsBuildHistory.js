import React, {Component} from 'react';
import PropTypes from 'prop-types';
import JenkinsBuildHistoryTable from "./JenkinsBuildHistoryTable";
import Layout from "./common/Layout";
import {Breadcrumb, Card} from "react-bootstrap";

class JenkinsBuildHistory extends Component {

    render() {
        let job = this.props.job;
        return (
            <React.Fragment>
                <Breadcrumb>
                    <Breadcrumb.Item href={`#/jobs/`}>Jobs</Breadcrumb.Item>
                    <Breadcrumb.Item active>{job}</Breadcrumb.Item>
                </Breadcrumb>
                <Layout withNavBar={false}>
                    <Card border="light" style={{width: '120rem'}}>
                        <Card.Body>
                            <Card.Title>Build history of {job}</Card.Title>
                            <JenkinsBuildHistoryTable {...this.props}/>
                        </Card.Body>
                    </Card>
                </Layout>

            </React.Fragment>
        )
    }
}

JenkinsBuildHistory.propTypes = {
    job: PropTypes.string,
    builds: PropTypes.array,
    loading: PropTypes.bool
};

export default JenkinsBuildHistory;
