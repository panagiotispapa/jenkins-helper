import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Breadcrumb, Card} from "react-bootstrap";
import JobsTable from "./JobsTable";

class Jobs extends Component {
    render() {
        return (
            <React.Fragment>
                <Breadcrumb>
                    <Breadcrumb.Item active>
                        Jobs
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Card border="light" style={{width: '100rem'}}>
                    <Card.Body>
                        <Card.Title>Jobs</Card.Title>
                        <JobsTable {...this.props}/>
                    </Card.Body>
                </Card>
            </React.Fragment>

        );
    }
}

Jobs.propTypes = {
    jobs: PropTypes.array,
    loading: PropTypes.bool
};

export default Jobs;