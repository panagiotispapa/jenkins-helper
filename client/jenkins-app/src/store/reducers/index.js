import {combineReducers} from 'redux-immutable';
import reRunBuildData from "./reRunBuildData";
import jobData from "./jobData";

const rootReducer = combineReducers({
    reRunBuildData: reRunBuildData,
    jobData: jobData,
});

export default rootReducer;