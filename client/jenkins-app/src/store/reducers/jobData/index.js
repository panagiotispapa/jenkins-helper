import * as actionTypes from '../../actions/actionTypes';
import {hide, show} from "../../../unit/utility";

let initState = {
    url: null,
    show: false
};

const jobData = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.JOB_STARTED:
      return show(action.data);
    case actionTypes.HIDE_JENKINS_JOB_STARTED_MODAL:
      return hide(state);
    default:
      return state;
  }
};

export default jobData;
