import * as actionTypes from '../../actions/actionTypes';
import {hide, show} from "../../../unit/utility";

let initState = {
    service: null,
    branch: null,
    skipTests: false,
    show: false
};

const reRunBuildData = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.JOB_RERUN_BUILD:
      return show(action.data);
    case actionTypes.HIDE_RE_RUN_MODAL:
      return hide(state);
    case actionTypes.RE_RUN_JENKINS_JOB:
      return hide(state);
    default:
      return state;
  }
};

export default reRunBuildData;
