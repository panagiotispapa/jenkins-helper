import * as actionTypes from "./actionTypes";
import {buildAction} from "../../unit/utility";
import JenkinsAPI from "../../api/JenkinsAPI";

export const hideReRunJobModal = () => buildAction(actionTypes.HIDE_RE_RUN_MODAL);

export const hideJenkinsJobModal = () => buildAction(actionTypes.HIDE_JENKINS_JOB_STARTED_MODAL);

export const startJenkinsJob = (jobName, params) => dispatch => {
    JenkinsAPI.startJob(jobName, params, jenkinsJobHasStarted(dispatch))
};

export const reRunJenkinsJob = (jobName, params) => dispatch => {
    dispatch(buildAction(actionTypes.RE_RUN_JENKINS_JOB));
    JenkinsAPI.startJob(jobName, params, jenkinsJobHasStarted(dispatch));
};

export const setReRunBuildData = (reRunBuildData) => buildAction(actionTypes.JOB_RERUN_BUILD, reRunBuildData);

export const jenkinsJobHasStarted = (dispatch) => resp => dispatch(buildAction(actionTypes.JOB_STARTED, resp.data));

