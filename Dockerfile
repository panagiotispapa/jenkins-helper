FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/jenkins-helper.jar app.jar
ENV JAVA_OPTS="-XX:+PrintFlagsFinal -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Xmx512m"

#https://developers.redhat.com/blog/2017/03/14/java-inside-docker/
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${REMOTE_DEBUG_PORT} -jar /app.jar